

class Card
{
	constructor(data, facedown)
	{
		this.data = data;
		this.facedown = facedown || false;
		this.graphic = this.make_sprite();
		this.hand = null;
	}


	get uuid()
	{
		if (this.data) {
			return this.data.uuid;
		} else {
			return undefined;
		}
	}

	try_sacrifice(sac_for)
	{
		networking.send_as_json({
			"type": "M_sac_card",
			"uuid": this.uuid,
			"sac_for": sac_for
		})
	}


	make_sprite(options)
	{
		if (this.facedown) {
			return this.make_cardback_sprite(options);
		} else {
			return this.make_card_sprite(options);
		}
	}


	make_card_sprite(options)
	{
		let texture = PIXI.utils.TextureCache[graphical.images.card];
		let card_sprite;
		if (options && options.existing_sprite) {
			card_sprite = options.existing_sprite;
			card_sprite.texture = texture;
		} else {
			card_sprite = new PIXI.Sprite(texture);
		}
		card_sprite.anchor.set(0.5, 0.5);
		card_sprite.card = this;
		let stats_ = [
			{ stat:this.data.desc, pos:[-90,50], options:{wordWrap:true, wordWrapWidth:180, fill:"white", fontSize:14}, anchor:[0,0] },
			{ stat:this.data.stats.atk, pos:[-75,0], options:{fill:"gold", fontSize:20} },
			{ stat:this.data.stats.speed, pos:[0,-5], options:{fill:"gold", fontSize:20} },
			{ stat:this.data.stats.hp, pos:[75,0], options:{fill:"gold", fontSize:20} },
			{ stat:this.data.name, pos:[40,-120], options:{fill:"white", fontSize:20, wordWrap:true, wordWrapWidth:100, align:"center"} },
			{ stat: this.data.type.toUpperCase() + (this.data.subtypes ? ": " + this.data.subtypes.join(', ') : ""),
				pos:[0,-80], options:{fill:"white", fontSize:12, wordWrap:true, wordWrapWidth:200, align:"center"} }
		]
		for (let i = 0; i < stats_.length; ++i) {
			let stat = stats_[i];
			if (stat.stat !== undefined) {
				let text = new PIXI.Text(""+stat.stat, stat.options);
				card_sprite.addChild(text);
				text.position.set(stat.pos[0], stat.pos[1]);
				let anchor = stat.anchor || [0.5, 0.5];
				text.anchor.set(anchor[0], anchor[1]);
			}
		}
		if (this.data.stats.gems) {
			let gem_container = new PIXI.Container();
			gem_container.position.set(-95, -155);
			card_sprite.addChild(gem_container);
			let gem_count = 0;
			for (let colour of Object.keys(this.data.stats.gems)) {
				let name = graphical.gems[colour];
				let gem_texture = PIXI.utils.TextureCache[graphical.images[name]];
				for (let i = this.data.stats.gems[colour]; i > 0; --i) {
					let gem = new PIXI.Sprite(gem_texture);
					gem.position.set(0, gem_count * 24);
					gem.anchor.set(0.5, 0);
					gem_container.addChild(gem);
					++gem_count;
				}
			}
			if (gem_count > 3) {
				gem_container.scale.set(0.5, 0.5);
			}
		}
		if (this.data.stats.cost) {
			let mana_container = new PIXI.Container();
			let mana_text = new PIXI.Text(this.data.stats.cost, {fill:"gold", fontSize:24});
			let flame = new PIXI.Sprite(PIXI.utils.TextureCache[graphical.images.mana]);
			mana_container.addChild(mana_text, flame);
			card_sprite.addChild(mana_container);
			mana_container.position.set(-60, -120);
			flame.anchor.set(0, 0.5);
			mana_text.anchor.set(1, 0.5);
		}
		card_sprite.interactive = true;
		card_sprite.interactiveChildren = false;
		card_sprite.buttonMode = true;

		return card_sprite;
	}


	make_cardback_sprite(options)
	{
		let texture = PIXI.utils.TextureCache[graphical.images.cardback];
		let card_sprite;
		if (options && options.existing_sprite) {
			card_sprite = options.existing_sprite;
			card_sprite.texture = texture;
		} else {
			card_sprite = new PIXI.Sprite(texture);
		}
		card_sprite.anchor.set(0.5, 0.5);
		card_sprite.card = this;

		return card_sprite;
	}


	make_draggable(drop_callback)
	{
		let card_sprite = this.graphic;
		card_sprite.is_held_down = false;
		card_sprite.drag_begin_coords = undefined;
		card_sprite.drag_begin_global_coords = undefined;
		card_sprite.dragged_pos = undefined;
		card_sprite.drop_callback = drop_callback;
		card_sprite.drag_stack_timeout = undefined;
		card_sprite.is_dragging_stack = false;

		card_sprite.on("mousedown", function(evt){
			card_sprite.is_held_down = true;
			card_sprite.drag_begin_pos = [card_sprite.position.x, card_sprite.position.y];
			card_sprite.drag_begin_stack_pos = [card_sprite.parent.position.x, card_sprite.parent.position.y];
			card_sprite.drag_begin_coords = [evt.data.originalEvent.clientX, evt.data.originalEvent.clientY];
			card_sprite.drag_begin_global_coords = card_sprite.getGlobalPosition();
			card_sprite.drag_stack_timeout = window.setTimeout(function() {
				card_sprite.is_dragging_stack = true;
			}, 500);
		});
		window.addEventListener("mouseup", function(evt){
			if (card_sprite.is_held_down) {
				let x = evt.clientX + card_sprite.drag_begin_global_coords.x - card_sprite.drag_begin_coords[0];
				let y = evt.clientY + card_sprite.drag_begin_global_coords.y - card_sprite.drag_begin_coords[1];
				card_sprite.is_held_down = false;
				if (card_sprite.drop_callback && card_sprite.drop_callback(card_sprite.card, x, y, card_sprite.is_dragging_stack)) {
					const parent = card_sprite.parent;
					parent.removeChild(card_sprite);
					parent.addChild(card_sprite);
				} else {
					card_sprite.position.set(card_sprite.drag_begin_pos[0], card_sprite.drag_begin_pos[1]);
					card_sprite.parent.position.set(card_sprite.drag_begin_stack_pos[0], card_sprite.drag_begin_stack_pos[1]);
				}
				card_sprite.drag_begin_coords = undefined;
				card_sprite.drag_begin_global_coords = undefined;
				card_sprite.drag_begin_pos = undefined;
				card_sprite.is_dragging_stack = false;
				if (card_sprite.drag_stack_timeout !== undefined) {
					window.clearTimeout(card_sprite.drag_stack_timeout);
					card_sprite.drag_stack_timeout = undefined;
				}
			}
		});
		card_sprite.on("mousemove", function(evt){
			if (card_sprite.is_held_down) {
				const x_diff = evt.data.originalEvent.clientX - card_sprite.drag_begin_coords[0];
				const y_diff = evt.data.originalEvent.clientY - card_sprite.drag_begin_coords[1];
				if (card_sprite.is_dragging_stack) {
					const new_x = card_sprite.drag_begin_stack_pos[0] + x_diff;
					const new_y = card_sprite.drag_begin_stack_pos[1] + y_diff;
					let stack = card_sprite.parent;
					stack.position.set(new_x, new_y);
				} else {
					const s = card_sprite.parent.scale.x;
					const new_x = card_sprite.drag_begin_pos[0] + x_diff;
					const new_y = card_sprite.drag_begin_pos[1] + y_diff;
					card_sprite.position.set(new_x/s, new_y/s);
					window.clearTimeout(card_sprite.drag_stack_timeout);
					card_sprite.drag_stack_timeout = undefined;
				}
			}
		});
	}


	put_to_hand(hand)
	{
		this.hand = hand;
		let card_sprite = this.graphic;
		card_sprite.selected = false;
		card_sprite.is_held_down = false;
		card_sprite.is_hovered = false;
		card_sprite.hovered_displacement = (this.hand.player.is_opponent ? [0,10] : [0,-10]);
		card_sprite.target_pos = [card_sprite.x, card_sprite.y];
		card_sprite.pos_move_rate = 1/10;
		card_sprite.my_glow = new PIXI.filters.GlowFilter(10, 4, 0, 0x00ff00, 0.1);
		card_sprite.my_glow.padding = 10;
		card_sprite.opponent_glow = new PIXI.filters.GlowFilter(10, 4, 0, 0xff0000, 0.1);
		card_sprite.opponent_glow.padding = 10;
		card_sprite.opponent_hovered = false;

		card_sprite.set_target_pos = function(x, y) {
			card_sprite.target_pos[0] = x;
			card_sprite.target_pos[1] = y;
		}
		card_sprite.set_target_x = function(x) {
			card_sprite.target_pos[0] = x;
		}
		card_sprite.set_target_y = function(y) {
			card_sprite.target_pos[1] = y;
		}
		card_sprite.anim_function = function(delta) {
			let x_diff = card_sprite.target_pos[0] - card_sprite.x;
			let y_diff = card_sprite.target_pos[1] - card_sprite.y;
			if (Math.abs(x_diff) > 0 || Math.abs(y_diff) > 0) {
				let speed = Math.min(1, delta * card_sprite.pos_move_rate);
				let new_x;
				if (Math.abs(x_diff) < 1) {
					new_x = card_sprite.target_pos[0];
				} else {
					new_x = card_sprite.x + x_diff * speed;
				}
				let new_y
				if (Math.abs(y_diff) < 1) {
					new_y = card_sprite.target_pos[1];
				} else {
					new_y = card_sprite.y + y_diff * speed;
				}
				card_sprite.position.set(new_x, new_y);
			}
		}
		graphical.register_animatable(card_sprite);

		card_sprite.opponent_hover = function() {
			if (!card_sprite.opponent_hovered) {
				card_sprite.filters = [card_sprite.opponent_glow];
				card_sprite.opponent_hovered = true;
				matchui.opponent_hovered_sprite = card_sprite;
				card_sprite.set_target_y(card_sprite.hovered_displacement[1]);
			}
		}
		card_sprite.opponent_endhover = function() {
			if (card_sprite.opponent_hovered) {
				if (!card_sprite.is_hovered) {
					card_sprite.filters = null;
					card_sprite.set_target_y(0);
				}
				card_sprite.opponent_hovered = false;
				if (matchui.opponent_hovered_sprite === card_sprite) {
					matchui.opponent_hovered_sprite = null;
				}
			}
		}

		card_sprite.update_post_mouse_event = function(evt) {
			if (!card_sprite.is_hovered && !card_sprite.is_held_down) {
				card_sprite.set_target_y(0);
			}
		}

		card_sprite.on("mouseover", function(evt){
			if (!card_sprite.is_hovered) {
				card_sprite.set_target_y(card_sprite.hovered_displacement[1]);
				card_sprite.filters = [card_sprite.my_glow];
				card_sprite.is_hovered = true;
				let index = -1;
				for (let card_c of card_sprite.card.hand.container.children) {
					++index;
					if (card_c.children[0] === card_sprite) {
						networking.send_as_json({"type":"P_hover","index":index});
					}
				}
			}
			card_sprite.update_post_mouse_event(evt);
		});
		card_sprite.on("mouseout", function(evt){
			if (!card_sprite.opponent_hovered) {
				card_sprite.filters = null;
				card_sprite.is_hovered = false;
				let index = -1;
				for (let card_c of card_sprite.card.hand.container.children) {
					++index;
					if (card_c.children[0] === card_sprite) {
						// this gets sent when you switch to another tab
						// so when testing you might want to comment out this line
						networking.send_as_json({"type":"P_endhover","index":index});
					}
				}
			}
			card_sprite.update_post_mouse_event(evt);
		});

		if (this.hand.player && !this.hand.player.is_opponent) {
			card_sprite.select = function() {
				card_sprite.tint = 0xff9966;
				card_sprite.selected = true;
				matchui.make_zoomed(card_sprite.card);
				if (matchui.can_sacrifice) {
					card_sprite.parent.addChild(matchui.main_hud.sac_container);
				}
			}
			card_sprite.deselect = function() {
				card_sprite.tint = 0xffffff;
				card_sprite.selected = false;
				matchui.unzoom();
				card_sprite.parent.removeChild(matchui.main_hud.sac_container);
			}
			card_sprite.on("click", function(evt){
				mainui.just_clicked = true;
				if (card_sprite.selected) {
					matchui.deselect_card();
				} else {
					matchui.select_card(card_sprite);
				}
			});
		}

	}



}

