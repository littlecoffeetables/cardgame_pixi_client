
// hexes use cube coordinates
// see: https://www.redblobgames.com/grids/hexagons/

const ADJ_HEXES = [[0,1,-1],[1,0,-1],[1,-1,0],[0,-1,1],[-1,0,1],[-1,1,0]];

function is_legit_coord(hex_coord)
{
	if (hex_coord[0] + hex_coord[1] + hex_coord[2] !== 0) {
		return false;
	}
	return true;
}

function hex_coords_to_xy(hex_coord) {
	let x = (hex_coord[0] - hex_coord[1]) * graphical.cos30;
	let y = -(hex_coord[0] + hex_coord[1]) * graphical.cos60 + hex_coord[2];
	return {x:x, y:y};
}



class Tile
{
	constructor(coords, scale)
	{
		this.coords = coords;
		// scale is "radius", ie distance to corner, in pixels
		this.scale = scale;
		this.unit = null;
		this.graphic;
	}

	init_graphic(container)
	{
		this.graphic = matchui.create_hex_tile_graphic(this, 0xffffff, 0x666666, 2);
		container.addChild(this.graphic);
		let xy = hex_coords_to_xy(this.coords);
		this.graphic.x = xy.x * this.scale;
		this.graphic.y = xy.y * this.scale;
	}

	get_offset_coords(coords)
	{
		return [this.coords[0]+coords[0], this.coords[1]+coords[1], this.coords[2]+coords[2]];
	}

	update_tint()
	{
		if (!this.unit) {
			this.graphic.tint = 0xffffff;
		} else if (this.unit.is_friendly) {
			this.graphic.tint = 0xddffdd;
		} else {
			this.graphic.tint = 0xcccccc;
		}
	}

	is_adjacent_with(tile)
	{
		if (tile === this) {
			return false;
		}
		if (Math.abs(this.coords[0] - tile.coords[0]) < 2 && Math.abs(this.coords[1] - tile.coords[1]) < 2 && Math.abs(this.coords[2] - tile.coords[2]) < 2) {
			return true;
		}
		return false;
	}

}





