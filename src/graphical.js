// interactions with PIXI

graphical = {};

graphical.cos30 = Math.cos(Math.PI / 6);
graphical.cos60 = Math.cos(Math.PI / 3);


graphical.create_button = function(text, fn, options)
{
	let buttonTexture = PIXI.utils.TextureCache[graphical.images.button];
	let button = new PIXI.Sprite(buttonTexture);
	button.anchor.set(0.5, 0.5);

	if (text) {
		button.text = new PIXI.Text(text, {fill:"white", fontSize:24});
		button.addChild(button.text);
		button.text.anchor.set(0.5, 0.5);
	}

	button.interactive = true;
	button.interactiveChildren = false;
	button.buttonMode = true;

	//button.__disabled = false;
	button.disable = function(edit_text) {
		//button.__disabled = true;
		button.interactive = false;
		button.tint = 0x333333; // should really have a separate texture
		if (edit_text && button.children.length > 0) {
			button.children[0].text = edit_text;
		}
	}
	button.enable = function(edit_text) {
		//button.__disabled = false;
		button.interactive = true;
		button.tint = 0xffffff;
		if (edit_text && button.children.length > 0) {
			button.children[0].text = edit_text;
		}
	}

	button.on("click", fn);

	button.on("mousedown", function(evt){
		button.is_held_down = true;
		button.texture = PIXI.utils.TextureCache[graphical.images.buttonDown];
	});
	window.addEventListener("mouseup", function(evt){
		button.is_held_down = false;
		if (button.is_hovered) {
			button.texture = PIXI.utils.TextureCache[graphical.images.buttonHover];
		} else {
			button.texture = PIXI.utils.TextureCache[graphical.images.button];
		}
	});
	button.on("mouseover", function(evt){
		button.is_hovered = true;
		if (button.is_held_down) {
			button.texture = PIXI.utils.TextureCache[graphical.images.buttonDown];
		} else {
			button.texture = PIXI.utils.TextureCache[graphical.images.buttonHover];
		}
	});
	button.on("mouseout", function(evt){
		button.is_hovered = false;
		if (button.is_held_down) {
			//button.texture = PIXI.utils.TextureCache[graphical.images.buttonDown];
		} else {
			button.texture = PIXI.utils.TextureCache[graphical.images.button];
		}
	});

	return button;
}




// game should automatically scale to take up all available space
graphical.resize_app = function() {
	app.renderer.resize(window.innerWidth, window.innerHeight);
	let scale_factor = Math.min(1, window.innerHeight / 800);
	for (let elem of graphical.repositionable) {
		elem.resize_reposition(window.innerWidth, window.innerHeight, scale_factor);
	}
}
graphical.auto_resize = function() {
	app.renderer.view.style.position = "absolute";
	app.renderer.view.style.display = "block";
	//app.renderer.autoResize = true;
	app.renderer.autoDensity = true;
	graphical.repositionable = [];
	window.onresize = graphical.resize_app;
	graphical.resize_app();
}
graphical.add_resize_reposition = function(elem, fn) {
	elem.resize_reposition = fn;
	graphical.repositionable.push(elem);
}

// do elements ever get removed from here or is this a memory leak?
// when are elem or elem.anim_function falsey?
graphical.animatable = [];
graphical.register_animatable = function(elem) {
	graphical.animatable.push(elem);
}
graphical.run_anim_functions = function(delta) {
	for (let i = graphical.animatable.length - 1; i >= 0; --i) {
		let elem = graphical.animatable[i];
		if (elem && elem.anim_function) {
			elem.anim_function(delta);
		} else {
			graphical.animatable.splice(i, 1);
		}
	}
}


// centering a sprite, "for testing"
graphical.centre = function(sprite) {
	sprite.position.set(window.innerWidth/2, window.innerHeight/2);
}

graphical.get_screen_w = function()
{
	return window.innerWidth;
}
graphical.get_screen_h = function()
{
	return window.innerHeight;
}

// load necessary images
graphical.images = {
	card: "img/card.png",
	cardback: "img/cardback.png",
	gem_b: "img/gem_b_1.png",
	gem_r: "img/gem_r_1.png",
	gem_g: "img/gem_g_1.png",
	gem_n: "img/gem_n_1.png",
	mana: "img/mana_flame.png",
	button: "img/button.png",
	buttonHover: "img/button_hover.png",
	buttonDown: "img/button_down.png",
	sac_back: "img/sac_back.png",
	sac_cards: "img/sac_cards.png",
	tile_highlighter: "img/hexhighlight.png",
	arrow: "img/arrow.png",
}
graphical.gems = {
	"neutral": "gem_n",
	"blue": "gem_b",
	"red": "gem_r",
	"green": "gem_g",
}

// initialize PIXI
var app = new PIXI.Application({});

{
	let img_paths = Object.values(graphical.images);
	PIXI.Loader.shared.add(img_paths).load(doneLoading);
}

// runs once all the images have loaded
function doneLoading()
{
	//console.log("Resources loaded!")
	app.ticker.add(delta => refresh(delta));
}

// runs up to 60 times/second
function refresh(delta)
{
	graphical.run_anim_functions(delta);
}




// reduce GPU/CPU usage when not focused
window.onblur = function () {
	app.ticker.stop();
};
window.onfocus = function () {
	app.ticker.start();
}
