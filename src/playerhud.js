

class PlayerHUD
{
	constructor(player)
	{
		this.player = player;
		this.container = new PIXI.Container();
		// whether to align to top or bottom of screen
		this.top = this.player.is_opponent;
		this._dir = (this.top ? 1 : -1);

		if (this.top) {
			graphical.add_resize_reposition(this.container, function(w, h) {
				this.position.set(0, 0);
			});
		} else {
			graphical.add_resize_reposition(this.container, function(w, h) {
				this.position.set(0, h);
			});
		}

		let mana_container = new PIXI.Container();
		this.container.addChild(mana_container);
		this.mana_text = new PIXI.Text("N/A", {fill:"gold", fontSize:32});
		let mana_flame = new PIXI.Sprite(PIXI.utils.TextureCache[graphical.images.mana]);
		mana_container.addChild(this.mana_text, mana_flame);
		mana_container.position.set(40, this._dir * 40);
		mana_flame.anchor.set(0, 0.5);
		this.mana_text.anchor.set(1, 0.5);
		this.update_mana();

		this.gem_container = new PIXI.Container();
		this.container.addChild(this.gem_container);
		this.gem_container.position.set(120, 0);
		this.gems = {};
		this.update_gems();

	}

	update_mana()
	{
		this.mana_text.text = "" + this.player.mana;
	}

	update_gems()
	{
		for (let gem_type of Object.keys(this.player.gems)) {
			let count = "" + this.player.gems[gem_type];
			if (this.gems[gem_type] === undefined) {
				let texture = PIXI.utils.TextureCache[graphical.images[graphical.gems[gem_type]]];
				let gem_sprite = new PIXI.Sprite(texture);
				let text = new PIXI.Text(count, {fill:"white", fontSize:24});
				text.anchor.set(1,0.5);
				gem_sprite.anchor.set(0,0.5);
				gem_sprite.addChild(text);
				this.gems[gem_type] = gem_sprite;
				gem_sprite.position.set(0, gem_sprite.height * this._dir * Object.keys(this.gems).length);
				this.gem_container.addChild(gem_sprite);
			}
			else {
				this.gems[gem_type].children[0].text = count;
			}
		}
	}


}

