

lobbyui = {};

lobbyui.press_play_button = function()
{
	if (lobby.nw.is_open) {
		lobbyui.deck_select = new Decklist(lobby.nw, lobbyui.select_deck);
		lobbyui.lobby_container.addChild(lobbyui.deck_select.container);
		graphical.centre(lobbyui.deck_select.container);
	} else {
		lobby.nw.try_reconnect();
	}
}

lobbyui.select_deck = function(deckid)
{
	if (deckid !== undefined) {
		lobby.send_enter_queue("standard", deckid);
	}
	lobbyui.lobby_container.removeChild(lobbyui.deck_select.container);
}

lobbyui.press_deckbuilder_button = function()
{
	deckbuilder.init(lobby.nw);
	mainui.switch_to_view(deckbuilderui);
}

lobbyui.init_lobby = function(lobby)
{
	lobbyui.lobby = lobby;
	lobbyui.lobby_container = new PIXI.Container();

	let play_button = graphical.create_button("Play", lobbyui.press_play_button);
	//graphical.centre(play_button);
	graphical.add_resize_reposition(play_button, function(w, h, s) {
		this.position.set(w/2,h/2);
	});
	lobbyui.lobby_container.addChild(play_button);

	let deckbuilder_button = graphical.create_button("Deckbuilder", lobbyui.press_deckbuilder_button);
	graphical.add_resize_reposition(deckbuilder_button, function(w, h, s) {
		this.position.set(w/2,h/2+100);
	});
	lobbyui.lobby_container.addChild(deckbuilder_button);

	lobbyui.connection_status_text = new PIXI.Text("", {fill:"red", fontSize:30});
	lobbyui.connection_status_text.position.set(0, 0);
	lobbyui.lobby_container.addChild(lobbyui.connection_status_text);

	lobbyui.queue_status_text = new PIXI.Text("", {fill:"white", fontSize:20});
	lobbyui.queue_status_text.anchor.set(1, 0);
	graphical.add_resize_reposition(lobbyui.queue_status_text, function(w, h, s) {
		this.position.set(w,0);
	});
	lobbyui.lobby_container.addChild(lobbyui.queue_status_text);

	app.stage.addChild(lobbyui.lobby_container);
	graphical.resize_app();
}

lobbyui.hide = function()
{
	app.stage.removeChild(lobbyui.lobby_container);
}

lobbyui.show = function()
{
	app.stage.addChild(lobbyui.lobby_container);
}
