

mainui = {};

mainui.globalClickHandler = function(evt)
{
	if (!mainui.just_clicked) {
		mainui.deselect_everything();
	}
	mainui.just_clicked = false;
}

mainui.deselect_everything = function()
{
	matchui.deselect_everything();
}

mainui.return_to_lobby = function()
{
	mainui.switch_to_view(lobbyui);
	match = undefined;
}

mainui.switch_to_view = function(view)
{
	for (let other of mainui.views) {
		other.hide();
	}
	view.show();
}

mainui.init = function()
{
	graphical.auto_resize();
	document.body.appendChild(app.view);
	app.stage.interactive = true;
	window.addEventListener("click", mainui.globalClickHandler);
	mainui.views = [matchui, lobbyui, deckbuilderui];
}
