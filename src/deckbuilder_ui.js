

deckbuilderui = {};
cards_by_cid = {};
card_stacks = {};

deckbuilderui.add_card_stack = function(card_stack, cv)
{
	if (card_stacks[cv] === undefined) {
		card_stacks[cv] = [card_stack];
	} else {
		card_stacks[cv].push(card_stack);
	}
}

deckbuilderui.press_load_deck = function()
{
	if (deckbuilderui.decklist !== undefined) {
		deckbuilderui.decklist.cleanup();
	}
	deckbuilderui.decklist = new Decklist(deckbuilder.nw, deckbuilderui.select_deck);
	deckbuilderui.container.addChild(deckbuilderui.decklist.container);
	graphical.centre(deckbuilderui.decklist.container);
}

deckbuilderui.press_save_deck = function()
{
	let cards = [];
	for (const card_stack of deckbuilderui.deck_container.children) {
		for (const card_sprite of card_stack.children) {
			let cid = card_sprite.card.cid;
			const global_pos = card_sprite.getGlobalPosition();
			let visual_pos = ""+global_pos.x+","+global_pos.y;
			cards.push( [cid, visual_pos] );
		}
	}
	let obj = {"type":"save_deck", "cards":cards};
	if (deckbuilderui.current_deck_id !== undefined) {
		obj["deckid"] = deckbuilderui.current_deck_id;
	} else {
		obj["name"] = prompt("Name your deck:");
	}
	deckbuilder.nw.send(JSON.stringify(obj));
}

deckbuilderui.press_clear_deck = function()
{
	deckbuilderui.clear_deck();
}

deckbuilderui.press_new_deck = function()
{
	deckbuilderui.clear_deck();
	deckbuilderui.set_current_deck(undefined);
}

deckbuilderui.press_rename_deck = function()
{
	let newname = prompt("Rename deck");
	if (newname !== null) {
		deckbuilder.nw.send(JSON.stringify({
			"type": "rename_deck",
			"deckid": deckbuilderui.current_deck_id,
			"name": newname
		}));
	}
}

deckbuilderui.press_delete_deck = function()
{
	if (confirm("Really delete deck?")) {
		deckbuilder.nw.send(JSON.stringify({
			"type": "delete_deck",
			"deckid": deckbuilderui.current_deck_id
		}));
		deckbuilderui.set_current_deck(undefined);
	}
}

deckbuilderui.press_organise_deck = function()
{
	// step 1: stack all cards in deck
	let stacks = deckbuilderui.deck_container.children.slice();
	for (let stack of stacks) {
		for (let card_sprite of stack.children) {
			deckbuilderui.rm_card_from_stack(card_sprite.card);
			deckbuilderui.put_to_stack_near(card_sprite.card, 0, 0, undefined);
		}
	}
	// step 2: move the stacks
	const stack_count = deckbuilderui.deck_container.children.length;
	const row_count = (stack_count > 5 ? 2 : 1);
	const stacks_per_row = parseInt(stack_count / row_count);
	const x_spacing = 250 * deckbuilderui.deckscale;
	const y_spacing = 250 * deckbuilderui.deckscale;
	const first_row_width = stacks_per_row * x_spacing;
	const second_row_width = (stack_count - stacks_per_row) * x_spacing;
	const centre_x = graphical.get_screen_w() / 2;
	const base_y = 450 + (row_count > 1 ? 0 : y_spacing/2);
	let i = 0;
	for (let stack of deckbuilderui.deck_container.children) {
		let x = centre_x;
		let y = base_y;
		if (i < stacks_per_row) {
			x -= first_row_width / 2;
			x += x_spacing * i;
		} else {
			x -= second_row_width / 2;
			x += x_spacing * (i - stacks_per_row);
			y += y_spacing;
		}
		stack.position.set(x, y);
		i++;
	}
}

deckbuilderui.press_add_all = function()
{
	i = 0;
	for (let card_stack of deckbuilderui.card_container.children.slice()) {
		if (card_stack.visible) {
			deckbuilderui.move_stack_to_deck(card_stack, 200+i*20, 400);
			i++;
		}
	}
}

deckbuilderui.deck_renamed = function(data)
{
	deckbuilderui.rename_deck_button.text.text = data["name"];
}

deckbuilderui.deck_saved = function(data)
{
	deckbuilderui.set_current_deck(data["deckid"], data["name"]);
}

deckbuilderui.set_current_deck = function(deckid, name)
{
	if (deckid === undefined) {
		deckbuilderui.rename_deck_button.visible = false;
		deckbuilderui.delete_deck_button.visible = false;
	} else {
		deckbuilderui.rename_deck_button.visible = true;
		deckbuilderui.delete_deck_button.visible = true;
		if (name !== undefined) {
			deckbuilderui.rename_deck_button.text.text = name;
		}
	}
	deckbuilderui.current_deck_id = deckid;
}

deckbuilderui.select_deck = function(deckid)
{
	deckbuilderui.decklist = undefined;
	if (deckid === undefined) {
		return;
	}
	deckbuilder.load_deck(deckid);
	deckbuilderui.clear_deck();
	deckbuilderui.set_current_deck(deckid);
}

deckbuilderui.load_deck = function(data)
{
	for (const [cid, cardid, version, visual_pos] of data["cards"]) {
		let card = cards_by_cid[cid];
		let xy = visual_pos.split(",");
		deckbuilderui.move_card_to_deck(card, +xy[0], +xy[1]);
	}
	deckbuilderui.rename_deck_button.text.text = data["name"];
}

deckbuilderui.clear_deck = function()
{
	const deck_copy = deckbuilderui.deck_container.children.slice();
	for (let card_stack of deck_copy) {
		deckbuilderui.move_stack_to_list(card_stack);
	}
}

deckbuilderui.try_init_cards = function()
{
	if (!deckbuilder.is_collection_loaded || !deckbuilder.are_all_cards_loaded) {
		return;
	}
	for (const [cid, cardid, version] of deckbuilder.collection) {
		let card_str = deckbuilder.get_card_str(cardid, version);
		let card = new Card(JSON.parse(card_str));
		card.cid = cid;
		card.cardid = cardid;
		card.version = version;

		card.make_draggable(deckbuilderui.drop_card);
		cards_by_cid[card.cid] = card;
		card.is_in_deck = false;
		deckbuilderui.put_to_stack_near(card);
	}
	graphical.resize_app();
}

// return false if the card should be returned to the drag start position, true otherwise
deckbuilderui.drop_card = function(card, x, y, in_stack_mode)
{
	let stack = card.graphic.parent;
	if (card.is_in_deck) {
		if (y < 300) {
			in_stack_mode ? deckbuilderui.move_stack_to_list(stack, x, y)
			: deckbuilderui.move_card_to_list(card, x, y);
		} else {
			if (!in_stack_mode) {
				deckbuilderui.rm_card_from_stack(card);
				deckbuilderui.put_to_stack_near(card, x, y, 50);
			}
		}
		return true;
	}
	else {
		if (y < 300) {
			return false;
		}
		else {
			in_stack_mode ? deckbuilderui.move_stack_to_deck(stack, x, y)
			: deckbuilderui.move_card_to_deck(card, x, y);
			return true;
		}
	}
}

deckbuilderui.realign_collection = function()
{
	let i = 0;
	for (let card_stack of deckbuilderui.card_container.children) {
		if (card_stack.visible) {
			card_stack.position.set(i * deckbuilderui.clspacing * deckbuilderui.clscale, 0);
			i++;
		}
	}
	deckbuilderui.shown_collection_length = i;
	deckbuilderui.move_cardlist(0);
}

deckbuilderui.rm_card_from_stack = function(card)
{
	let stack = card.graphic.parent;
	stack.removeChild(card.graphic);
	if (stack.children.length === 0) {
		const cv = [card.cardid, card.version];
		const index = card_stacks[cv].indexOf(stack);
		if (index > -1) {
			card_stacks[cv].splice(index, 1);
		}
		stack.parent.removeChild(stack);
		if (!card.is_in_deck) {
			deckbuilderui.realign_collection();
		}
	} else {
		let y = 0;
		for (let card_sprite of stack.children) {
			card_sprite.position.y = y;
			y += 20;
		}
	}
}

deckbuilderui.put_to_stack_near = function(card, x, y, max_distance)
{
	none_found = true;
	const cv = [card.cardid, card.version];
	if (card_stacks[cv] === undefined) {
		card_stacks[cv] = [];
	}
	for (let stack of card_stacks[cv]) {
		if (!card.is_in_deck && stack.parent !== deckbuilderui.card_container) continue;
		if (card.is_in_deck) {
			if (stack.parent !== deckbuilderui.deck_container) continue;
			// max_distance only applies in deck
			const x_diff = Math.abs(x - stack.position.x);
			const y_diff = Math.abs(y - stack.position.y);
			if (x_diff > max_distance || y_diff > max_distance) continue;
		}
		none_found = false;
		card.graphic.position.set(0, stack.children.length * 20);
		stack.addChild(card.graphic);
		break;
	}
	if (none_found) {
		let stack = new PIXI.Container();
		if (card.is_in_deck) {
			stack.position.set(x, y);
			stack.scale.set(deckbuilderui.deckscale);
			deckbuilderui.deck_container.addChild(stack);
		} else {
			// todo: move the stack to the right position in the card list
			stack.position.set(deckbuilderui.card_container.children.length * deckbuilderui.clspacing * deckbuilderui.clscale, 0);
			stack.scale.set(deckbuilderui.clscale);
			deckbuilderui.card_container.addChild(stack);
			deckbuilderui.shown_collection_length += 1;
		}
		stack.addChild(card.graphic);
		deckbuilderui.add_card_stack(stack, cv);
		card.graphic.position.set(0,0);
	}
}

deckbuilderui.move_stack_to_deck = function(stack, x, y)
{
	// this is not super efficient but way easier than the alternative
	for (let card_sprite of stack.children.slice()) {
		let card = card_sprite.card;
		deckbuilderui.move_card_to_deck(card, x, y);
	}
}

deckbuilderui.move_stack_to_list = function(stack, x, y)
{
	// same issue as above
	for (let card_sprite of stack.children.slice()) {
		let card = card_sprite.card;
		deckbuilderui.move_card_to_list(card, x, y);
	}
}

deckbuilderui.move_card_to_deck = function(card, x, y)
{
	deckbuilderui.rm_card_from_stack(card);
	card.is_in_deck = true;
	deckbuilderui.put_to_stack_near(card, x, y, 50);
}

deckbuilderui.move_card_to_list = function(card, x, y)
{
	deckbuilderui.rm_card_from_stack(card);
	card.is_in_deck = false;
	deckbuilderui.put_to_stack_near(card, x, y);
}

deckbuilderui.press_lobby = function()
{
	mainui.return_to_lobby();
}

deckbuilderui.on_key_down = function(evt)
{
	if (evt.key === "Escape") {
		if (deckbuilderui.decklist !== undefined) {
			deckbuilderui.decklist.close();
		} else {
			deckbuilderui.press_lobby();
		}
	}
}

deckbuilderui.filteredit = function(evt) {
	if (deckbuilderui.refiltertimeout !== undefined) {
		window.clearTimeout(deckbuilderui.refiltertimeout);
	}
	deckbuilderui.refiltertimeout = window.setTimeout(deckbuilderui.runfilter, 300);
}

deckbuilderui.show_all_cards = function(realign=true)
{
	for (let card_stack of deckbuilderui.card_container.children) {
		card_stack.visible = true;
	}
	if (realign) deckbuilderui.realign_collection();
}

deckbuilderui.runfilter = function() {
	deckbuilderui.show_all_cards(false);
	filtertext = filter_input.value.toLowerCase();
	let parts = filtertext.split(" ");
	for (let i = 0; i < parts.length; i++) {
		let part = parts[i];
		let split_part = part.split(":");
		if (split_part.length === 1) {
			parts[i] = ["", split_part];
		} else {
			parts[i] = split_part;
		}
	}
	for (let card_stack of deckbuilderui.card_container.children) {
		for (const part of parts) {
			if (card_stack.children.length === 0) {
				console.warning("card stack is empty");
				card_stack.visible = false;
				break;
			}
			const data = card_stack.children[0].card.data;
			if (data === undefined) {
				console.error("card data is undefined");
				card_stack.visible = false;
				break;
			}
			const filter = deckbuilderui.filter_funcs[part[0]];
			if ( filter === undefined || !filter(part[1], data) )
			{
				card_stack.visible = false;
				break;
			}
		}
	}
	deckbuilderui.realign_collection();
}

deckbuilderui.filter_name = function(input, data)
{
	return data.name && data.name.toLowerCase().indexOf(input) >= 0;
}

deckbuilderui.filter_desc = function(input, data)
{
	return data.desc && data.desc.toLowerCase().indexOf(input) >= 0;
}

deckbuilderui.filter_type = function(input, data)
{
	return data.type && data.type.toLowerCase().indexOf(input) >= 0;
}

deckbuilderui.filter_flavor = function(input, data)
{
	return data.flavor && data.flavor.toLowerCase().indexOf(input) >= 0;
}

deckbuilderui.filter_subtype = function(input, data)
{
	if (!data.subtypes) return false;
	for (const subtype of data.subtypes) {
		if (subtype.toLowerCase().indexOf(input) >= 0) return true;
	}
	return false;
}

// todo: allow appending +/- to compare >=/<=
deckbuilderui.filter_cost = function(input, data)
{
	return data.stats && data.stats.cost !== undefined && data.stats.cost === parseInt(input);
}

const gems_abbreviations = {
	"r":"red","g":"green","b":"blue","n":"neutral",
}
// todo: make this more versatile overall
deckbuilderui.filter_gems = function(input, data)
{
	if (!data.stats || !data.stats.gems) return false;
	let mod = 1;
	for (const letter of input) {
		const int = parseInt(letter);
		console.log(input, data, int, letter);
		if (int !== int) {
			// not a number
			if (gems_abbreviations[letter] === undefined) return false;
			const value = data.stats.gems[gems_abbreviations[letter]] || 0;
			if (value < mod) return false;
			mod = 1;
		} else {
			mod = int;
		}
	}
	return true;
}

deckbuilderui.filter_funcs = {
	"": deckbuilderui.filter_name,
	"name": deckbuilderui.filter_name,
	"d": deckbuilderui.filter_desc,
	"desc": deckbuilderui.filter_desc,
	"description": deckbuilderui.filter_desc,
	"t": deckbuilderui.filter_type,
	"type": deckbuilderui.filter_type,
	"s": deckbuilderui.filter_subtype,
	"subtype": deckbuilderui.filter_subtype,
	"f": deckbuilderui.filter_flavor,
	"flavor": deckbuilderui.filter_flavor,
	"lore": deckbuilderui.filter_flavor,
	"c": deckbuilderui.filter_cost,
	"cost": deckbuilderui.filter_cost,
	"gems": deckbuilderui.filter_gems,
	"g": deckbuilderui.filter_gems,
/*	"atk": deckbuilderui.filter_atk,
	"attack": deckbuilderui.filter_atk,
	"speed": deckbuilderui.filter_speed,
	"hp": deckbuilderui.filter_hp,
	"health": deckbuilderui.filter_hp,*/
}

deckbuilderui.move_cardlist = function(delta)
{
	let old_offset = deckbuilderui.card_list_offset;
	let new_offset = old_offset - 100 * delta;
	let min_offset = - deckbuilderui.shown_collection_length * deckbuilderui.clscale * deckbuilderui.clspacing;
	let max_offset = 0;
	deckbuilderui.card_list_offset = Math.max(min_offset, Math.min(new_offset, max_offset));
	deckbuilderui.card_container.position.x += deckbuilderui.card_list_offset - old_offset;
}

deckbuilderui.init = function()
{
	deckbuilderui.card_list_offset = 0;
	deckbuilderui.shown_collection_length = 0;
	deckbuilderui.card_container = new PIXI.Container();
	deckbuilderui.clscale = 0.7;
	deckbuilderui.clspacing = 200;
	//deckbuilderui.card_container.scale.set(0.7);
	graphical.add_resize_reposition(deckbuilderui.card_container, function(w,h,s) {
		this.position.set(deckbuilderui.card_list_offset + w/2, 200);
	});
	document.addEventListener("wheel", function(evt) {
		deckbuilderui.move_cardlist(evt.deltaY);
	});

	deckbuilderui.deck_container = new PIXI.Container();
	graphical.add_resize_reposition(deckbuilderui.deck_container, function(w,h,s) {
		for (const card_sprite of this.children) {
			card_sprite.position.x = Math.min(w, Math.max(0, card_sprite.position.x));
			card_sprite.position.y = Math.min(h, Math.max(0, card_sprite.position.y));
		}
	});
	deckbuilderui.deckscale = 0.8;

	deckbuilderui.buttons_row = new PIXI.Container();
	deckbuilderui.container = new PIXI.Container();
	deckbuilderui.container.addChild(deckbuilderui.card_container);
	deckbuilderui.container.addChild(deckbuilderui.deck_container);
	deckbuilderui.container.addChild(deckbuilderui.buttons_row);
	deckbuilderui.buttons_row.scale.set(0.5);

	let buttons = [
		{name: "load_deck", text: "Load deck", row:2},
		{name: "save_deck", text: "Save deck", row:2},
		{name: "clear_deck", text: "Clear deck", row:2},
		{name: "new_deck", text: "New deck", row:2},
		{name: "rename_deck", text: "---", row:2},
		{name: "delete_deck", text: "Delete deck", row:2},
		{name: "lobby", text: "Back to lobby", row:1},
		{name: "organise_deck", text: "Organise deck", row:1},
		{name: "add_all", text: "Add all to deck", row:1},
	]
	const row_counts = {}
	for (let button of buttons) {
		let new_button = graphical.create_button(button.text, deckbuilderui["press_"+button.name]);
		deckbuilderui.buttons_row.addChild(new_button);
		if (row_counts[button.row] === undefined) row_counts[button.row] = 0;
		new_button.position.set(row_counts[button.row] * 240 + 120, button.row * 70);
		row_counts[button.row] += 1;
		deckbuilderui[button.name + "_button"] = new_button;
	}
	deckbuilderui.rename_deck_button.visible = false;
	deckbuilderui.delete_deck_button.visible = false;

	window.addEventListener("keydown", deckbuilderui.on_key_down);
	filterbox.addEventListener("input", deckbuilderui.filteredit);

	graphical.resize_app();
}

deckbuilderui.hide = function()
{
	filterbox.classList.add("hidden");
	app.stage.removeChild(deckbuilderui.container);
}

deckbuilderui.show = function()
{
	filterbox.classList.remove("hidden");
	app.stage.addChild(deckbuilderui.container);
}
