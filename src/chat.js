

var chat = {};

// I think I'll make the server handle this but still
chat.escapeHTML = function(unsafe_str) {
    return unsafe_str
      .replace(/&/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      .replace(/\"/g, '&quot;')
      .replace(/\'/g, '&#39;')
      .replace(/\//g, '&#x2F;')
}


chat.focus = function() {
	chatbox.classList.add("focused");
}
chat.unfocus = function() {
	chatbox.classList.remove("focused");
	chat_input.blur();
}

window.addEventListener("keydown", function(evt) {
	if (document.activeElement !== document.body) {
		return;
	}
	if (evt.key.toLowerCase() === "t") {
		chat.focus();
		window.setTimeout(function(evt) {
			chat_input.focus();
			chat_input.value = chat_input.value;
		}, 1);
	}
});

window.addEventListener("click", function(evt) {
	if (evt.target && evt.target.id === "chat_input") {
		chat.focus();
	} else {
		chat.unfocus();
	}
});


chat.scrolled = function() {
	chat.isScrolledToBottom = messagebox.scrollHeight - messagebox.clientHeight <= messagebox.scrollTop + 1;
}

chat.update_scroll = function(){
	if (chat.isScrolledToBottom) {
	    messagebox.scrollTop = messagebox.scrollHeight;
	}
}

chat.add_message = function(text, author) {
	let message = document.createElement("p");
	message.innerHTML = '<span class="author">' + author + '</span> ' + text;
	chat_messages.appendChild(message);
	chat.update_scroll();
}
chat.handle_message = function(data)
{
	let author = data.author || "[Unknown]";
	chat.add_message(data.message, author);
}

chat.send_message = function(message)
{
	if (chat.nw) {
		let data = JSON.stringify({type:"chat_message",message:message})
		chat.nw.send(data);
	}
}

chat.init = function(networking)
{
	chat.nw = networking;
	chat.nw.set_data_handler("chat_message", chat.handle_message);

	chat_input.addEventListener("keydown", function(evt) {
		if (evt.key === "Enter") {
			chat.send_message(chat_input.value);
			chat_input.value = "";
		}
		else if (evt.key === "Escape") {
			chat_input.value = "";
			chat.unfocus();
		}
	});
	messagebox.addEventListener("scroll", function(evt){
		chat.scrolled();
	});
	chat.scrolled();

}

