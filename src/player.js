// one of the two players in a match


class Player
{
	constructor(is_opponent, player)
	{
		this.is_opponent = is_opponent;
		// 0 or 1
		this.player = player;
		this.mana = NaN;
		this.gems = {};
		this.hand = new Hand(this);
		this.hud = new PlayerHUD(this);
	}

	get hand_size()
	{
		return this.hand.size();
	}

	get_gems(type)
	{
		return this.gems[type] || 0;
	}

	/*add_gems(type, amount)
	{
		if (this.gems[type]) {
			this.gems[type] += amount;
		} else {
			this.gems[type] = amount;
		}
	}*/

	update_resources(mana, gems)
	{
		if (mana !== undefined) {
			this.mana = mana;
			this.hud.update_mana();
		}
		if (gems !== undefined) {
			this.gems = gems;
			this.hud.update_gems();
		}
	}

	add_card(card_str, i)
	{
		let card_data = JSON.parse(card_str);
		this.hand.make_and_add_card(card_data, i);
	}

	draw_cards(data)
	{
		if (this.is_opponent) {
			if (data.count) {
				for (let i = 0; i < data.count; ++i) {
					this.hand.make_and_add_card(undefined);
				}
			}
		} else {
			if (data.card) {
				this.add_card(data.card);
			} else if (data.cards) {
				let i = 0;
				for (let card of data.cards) {
					this.add_card(card, i);
					++i;
				}
			}
		}
	}

	undraw_card(data)
	{
		let sacced_for = data.sacced_for;
		if (this.is_opponent) {
			if (data.count) {
				for (let i = 0; i < data.count; ++i) {
					this.hand.rm_card(undefined);
				}
			}
		}
		else {
			if (data.uuid !== undefined) {
				this.hand.rm_card(data.uuid);
			} else if (data.uuids) {
				for (let uuid of data.uuids) {
					this.hand.rm_card(uuid);
				}
			}
			if (sacced_for !== undefined) {
				matchui.can_sacrifice = false;
			}
		}
	}

}
