

deckbuilder = {};
card_registry = {};

deckbuilder.cards_loaded = function(data)
{
	console.log("cards_loaded");
	// [ [cid, cardid, version] ]
	deckbuilder.collection = data["cards"];
	deckbuilder.is_collection_loaded = true;
	deckbuilderui.try_init_cards();
}

deckbuilder.all_cards_loaded = function(data)
{
	console.log("all_cards_loaded");
	for (const [cardid, versions] of Object.entries(data["cards"])) {
		card_registry[cardid] = versions;
	}
	deckbuilder.are_all_cards_loaded = true;
	deckbuilderui.try_init_cards();
}

deckbuilder.get_card_str = function(cardid, version)
{
	return card_registry[cardid][version-1];
}

deckbuilder.deck_loaded = function(data)
{
	deckbuilderui.load_deck(data);
}

deckbuilder.load_deck = function(deckid)
{
	data = JSON.stringify({"type":"load_deck", "deckid":deckid});
	deckbuilder.nw.send(data);
}

/*deckbuilder.decklist_loaded = function(data)
{
	console.log("decklist_loaded");
}*/

deckbuilder.deck_saved = function(data)
{
	deckbuilderui.deck_saved(data);
}

deckbuilder.deck_save_failed = function(data)
{
	console.log("deck_save_failed");
}

deckbuilder.deck_renamed = function(data)
{
	deckbuilderui.deck_renamed(data);
}

deckbuilder.deck_rename_failed = function(data)
{
	console.log("deck_rename_failed");
}

deckbuilder.deck_deleted = function(data)
{
	console.log("deck_deleted");
}

deckbuilder.deck_delete_failed = function(data)
{
	console.log("deck_delete_failed");
}


deckbuilder.init = function(networking)
{
	if (deckbuilder.nw !== undefined) {
		return;
	}
	deckbuilder.nw = networking;
	deckbuilder.nw.set_data_handler("cards_loaded", deckbuilder.cards_loaded);
	deckbuilder.nw.set_data_handler("all_cards_loaded", deckbuilder.all_cards_loaded);
	deckbuilder.nw.set_data_handler("deck_loaded", deckbuilder.deck_loaded);
	//deckbuilder.nw.set_data_handler("decklist_loaded", deckbuilder.decklist_loaded);
	deckbuilder.nw.set_data_handler("deck_saved", deckbuilder.deck_saved);
	deckbuilder.nw.set_data_handler("deck_save_failed", deckbuilder.deck_save_failed);
	deckbuilder.nw.set_data_handler("deck_renamed", deckbuilder.deck_renamed);
	deckbuilder.nw.set_data_handler("deck_rename_failed", deckbuilder.deck_rename_failed);
	deckbuilder.nw.set_data_handler("deck_deleted", deckbuilder.deck_deleted);
	deckbuilder.nw.set_data_handler("deck_delete_failed", deckbuilder.deck_delete_failed);
	deckbuilderui.init();
	deckbuilder.nw.send(JSON.stringify({"type":"list_cards"}));
	deckbuilder.nw.send(JSON.stringify({"type":"load_all_cards"}));
}

