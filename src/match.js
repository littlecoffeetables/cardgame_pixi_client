// an individual game between two players


class Match
{
	constructor(data)
	{
		this.board = new GameBoard();
		this.my_player_number = data.players[login.user_id];
		this.you = new Player(false, this.my_player_number);
		this.opponent = new Player(true, 1-this.my_player_number);
		if (this.my_player_number === 1) {
			this.players = [this.opponent, this.you];
			this.inverted = true;
		} else {
			this.players = [this.you, this.opponent];
			this.inverted = false;
		}
		this.current_player = 0;
		this.init_handlers();
	}

	is_my_turn()
	{
		return this.my_player_number === this.current_player;
	}

	ondisconnect()
	{
		matchui.end_match("dc");
	}

	end_turn()
	{
		networking.send_as_json({"type":"M_endturn"});
	}

	init_handlers()
	{
		networking.set_data_handler("M_draw_card", this.draw_card.bind(this));
		networking.set_data_handler("M_rm_card", this.rm_card.bind(this));
		networking.set_data_handler("M_event_unit_appeared", this.summon_unit.bind(this));
		networking.set_data_handler("M_event_unit_disappeared", this.remove_unit.bind(this));
		networking.set_data_handler("M_resource_update", this.update_resources.bind(this));
		networking.set_data_handler("M_change_turn", this.change_turn.bind(this));
		networking.set_data_handler("M_event_unit_moved", this.unit_moved.bind(this));
		networking.set_data_handler("M_event_stats_changed", this.unit_stats_changed.bind(this));
		networking.set_data_handler("M_match_over", this.match_over.bind(this));
	}

	match_over(data)
	{
		matchui.end_match(data.result);
	}

	change_turn(data)
	{
		if (data.to !== undefined) {
			this.current_player = data.to;
		}
		matchui.change_turn()
	}

	draw_card(data)
	{
		if (data.player === undefined) {
			console.error("Got no player")
		}
		let player = this.players[data.player];
		player.draw_cards(data);
	}

	rm_card(data)
	{
		if (data.player === undefined) {
			if (data.uuid !== undefined) {
				data.player = this.my_player_number;
			}
		}
		let player = this.players[data.player];
		player.undraw_card(data);
	}

	update_resources(data)
	{
		if (data.player === undefined) {
			console.error("got no player");
			return;
		}
		else if (data.player === "all") {
			for (let player_id = 0; player_id < this.players.length; ++player_id) {
				this.players[player_id].update_resources(data.mana[player_id], data.gems[player_id]);
			}
		}
		else {
			let player = this.players[data.player];
			player.update_resources(data.mana, data.gems);
		}
	}

	summon_unit(data)
	{
		let unit_data = JSON.parse(data.unit);
		let card_data = JSON.parse(unit_data.card);
		let tile = this.board.tiles[unit_data.coords];
		let card = new Card(card_data);
		tile.unit = new Unit(card, unit_data.owner, tile, unit_data.unit_stats);
	}

	remove_unit(data)
	{
		let unit = this.board.units[data.unit];
		unit.destroy();
	}

	unit_moved(data)
	{
		let from_tile = this.board.tiles[data.from];
		let to_tile = this.board.tiles[data.to];
		let unit = from_tile.unit;
		if (unit.uuid !== data.uuid) {
			return;
		}
		unit.move(from_tile, to_tile);
	}

	unit_stats_changed(data)
	{
		let unit = this.board.units[data.unit];
		unit.stat_change(data);
	}

}
