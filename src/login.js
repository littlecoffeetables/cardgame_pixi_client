

var login = {};

login.handle_error = function(data)
{
	login_error_message.innerHTML = data.error;
}

login.handle_login = function(data)
{
	login_error_message.innerHTML = "";
	loginbox.classList.add("hidden");
	chatbox.classList.remove("hidden");
	if (data.motd) {
		console.log("motd: "+data.motd);
	}
	if (data.user_id) {
		login.user_id = data.user_id;
	}
	login.callback();
}

login.handle_register = function(data)
{
	login_error_message.innerHTML = "Success registering account " + data.username;
	login_button.focus();
}

login.enter = function()
{
	login_error_message.innerHTML = "";
	networking.try_connect(login_address_input.value, login_port_input.value);
	login_button.disabled = true;
	login.next_action = "login";
	window.setTimeout( function(){
		login_button.disabled = false;
	}, 500);
}

login.register = function()
{
	login_error_message.innerHTML = "";
	networking.try_connect(login_address_input.value, login_port_input.value);
	register_button.disabled = true;
	login.next_action = "register";
	window.setTimeout( function(){
		register_button.disabled = false;
	}, 500);
}

login.onconnect = function()
{
	login.username = login_username_input.value;
	login.password = login_password_input.value;
	if (login.next_action === "login") {
		login.send_login(login.username, login.password);
	} else if (login.next_action === "register") {
		login.send_register(login.username, login.password);
	}
}

login.onerror = function()
{
	login_error_message.innerHTML = "Unable to connect";
}

// for testing
window.addEventListener("load", function(){
	cb = function(evt) {
		if (evt.key === "Enter") {
			login.enter();
		}
	}
	login_username_input.addEventListener("keydown", cb);
	login_password_input.addEventListener("keydown", cb);
});

login.send_login = function(username, password)
{
	if (login.nw) {
		let data = JSON.stringify({
			type: "login",
			name: username,
			password: password
		})
		login.nw.send(data);
	}
}

login.send_register = function(username, password)
{
	if (login.nw) {
		let data = JSON.stringify({
			type: "register",
			name: username,
			password: password
		})
		login.nw.send(data);
	}
}

login.init = function(networking, callback)
{
	login.nw = networking;
	login.nw.set_data_handler("login_success", login.handle_login);
	login.nw.set_data_handler("login_error", login.handle_error);
	login.nw.set_data_handler("register_success", login.handle_register);
	login.callback = callback;
}

