

class Networking
{
	constructor(address, port)
	{
		this._data_handlers = {};
		this.debug = true;
		this.verbose_debug = false;
		this.log_peer_data = false;
		this._debug_log = [];
		this.last_uri;
	}

	get is_open()
	{
		return this.ws && this.ws.readyState === WebSocket.OPEN;
	}

	try_connect(address, port)
	{
		address = address || window.location.hostname || "localhost";
		port = port || 8081;
		let uri = "ws://" + address + ":" + port;
		this.do_connect(uri);
		this.last_uri = uri;
	}

	do_connect(uri)
	{
		console.log("trying to connect to " + uri)
		this.ws = new WebSocket(uri);
		if (this.connected) this.ws.onopen = this.connected.bind(this);
		if (this.errored) this.ws.onerror = this.errored.bind(this);
		if (this.closed) this.ws.onclose = this.closed.bind(this);
		this.ws.onmessage = this.recv.bind(this);
	}

	try_reconnect()
	{
		this.do_connect(this.last_uri);
	}

	// TODO
	connected(evt) {
		console.log("connected");
		login.onconnect();
		if (lobby.nw) {
			lobby.onreconnect();
		}
	}
	closed(evt) {
		console.log("closed");
		if (match) {
			match.ondisconnect();
		}
		if (lobby.nw) {
			lobby.ondisconnect();
		}
	}
	errored(evt) {
		console.log("errored");
		if (!lobby.nw) {
			login.onerror();
		}
	}

	recv(evt)
	{
		this._log("received", evt.data);
		let data;
		try {
			data = JSON.parse(evt.data);
		}
		catch (SyntaxError) {
			console.log("data is invalid");
			data = null;
		}
		if (data) {
			if (data.type === "M_bundle") {
				for (let sub_data of data.bundle) {
					this.handle_data(sub_data);
				}
			} else {
				this.handle_data(data);
			}
		}
	}

	handle_data(data)
	{
		if (data.type && this._data_handlers[data.type]) {
			this._data_handlers[data.type](data);
		} else {
			this._log("no handler for data of type", data.type);
			console.log("no handler for data of type", data.type);
		}
	}

	send_as_json(data)
	{
		this.send(JSON.stringify(data));
	}

	send(data)
	{
		if (this.is_open) {
			this._log("sending", data);
			this.ws.send(data);
		} else {
			console.error("trying to send data when websocket is closed:", data);
		}
	}

	_log(...messages)
	{
		if (this.debug) {
			let time = new Date();
			let string = "[" + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds() + "." + time.getMilliseconds() + "]";
			let loggable = true;
			for (let msg of messages) {
				try {
					let data = JSON.parse(msg)
					if (!this.log_peer_data && data.type && data.type.substr(0,2) === "P_") {
						loggable = false;
					}
				} catch (e) {}
				string += " " + msg;
			}
			if (loggable) {
				this._debug_log.push(string);
			}
			if (this.verbose_debug) {
				console.log(string);
			}
		}
	}

	print_log(max_count)
	{
		let start_index = 0;
		let j = this._debug_log.length;
		if (max_count) {
			start_index = j - max_count;
		}
		for (let i = start_index; i < j; ++i) {
			console.log(this._debug_log[i]);
		}
	}

	set_data_handler(type, fn)
	{
		this._log("setting handler for type", type)
		this._data_handlers[type] = fn;
	}

}




