

class Decklist
{
	constructor(networking, callback)
	{
		this.callback = callback;
		this.container = new PIXI.Container();
		this.title = new PIXI.Text("loading decklist...", {"fill":"white"});
		this.title.anchor.set(0.5, 0.5);
		this.container.addChild(this.title);
		networking.set_data_handler("decklist_loaded", this.loaded.bind(this));
		let data = JSON.stringify({"type": "list_decks"});
		networking.send(data);
	}

	loaded(data)
	{
		if (data["decks"].length === 0) {
			console.warn("Got 0 decks");
		}
		this.title.text = "Select a deck";
		this.title.position.y = -100;
		let x_offset = 0;
		let y_offset = 0;
		for (let deck of data["decks"]) {
			let deckid = deck[0];
			let deckname = deck[1];
			let deck_button = graphical.create_button(deckname, this.select_deck.bind(this, deckid));
			this.container.addChild(deck_button);
			deck_button.position.set(x_offset, y_offset);
			y_offset += 90;
		}
	}

	cleanup()
	{
		this.container.destroy({"children":true});
		this.callback = undefined;
	}

	select_deck(deckid)
	{
		this.callback(deckid);
		this.cleanup();
	}

	close()
	{
		this.select_deck(undefined);
	}
}
