// waiting room for matchmaking (and chat)

var lobby = {};

lobby.handle_begin_match = function(data)
{
	console.log("hype!")
	lobbyui.queue_status_text.text = "";
	if (data.gamemode && lobby.callback) {
		lobby.callback(data);
	}
}

// idk how this could end up getting triggered but you know
lobby.handle_opponent_cancelled = function(data)
{
	console.log("Opponent cancelled");
}

lobby.handle_queue_entered = function(data)
{
	console.log("Queueing");
	lobbyui.queue_status_text.text = "Queueing...";
}

lobby.send_enter_queue = function(type, deckid)
{
	if (lobby.nw) {
		let data = JSON.stringify({
			type:"enter_queue",
			gamemode:type,
			deck:deckid
		});
		lobby.nw.send(data);
	}
}

lobby.send_leave_queue = function(type)
{
	if (lobby.nw) {
		let data = JSON.stringify({type:"leave_queue", gamemode:type});
		lobby.nw.send(data);
	}
}

lobby.init = function(networking, callback)
{
	lobby.nw = networking;
	lobby.callback = callback;
	lobby.nw.set_data_handler("begin_match", lobby.handle_begin_match);
	lobby.nw.set_data_handler("opponent_cancel", lobby.handle_opponent_cancelled);
	lobby.nw.set_data_handler("success_enter_queue", lobby.handle_queue_entered);
}

lobby.ondisconnect = function()
{
	lobbyui.connection_status_text.text = "Connection lost";
	lobbyui.queue_status_text.text = "";
}

lobby.onreconnect = function()
{
	lobbyui.connection_status_text.text = "";
}

