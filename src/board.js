

class GameBoard
{
	constructor()
	{
		this.all_hex_coords = [];
		this.tile_container = new PIXI.Container();
		this.init_board();
		this.tiles;
		this.units = {};
	}

	init_board()
	{
		this.tiles = {};

		let to_add = [[0,0,0]];
		while (to_add.length > 0) {

			let next = to_add.pop();
			if (this.tiles[next] !== undefined) {
				continue;
			}
			this.all_hex_coords.push(next);

			let tile = new Tile(next, 50);
			this.tiles[next] = tile;
			tile.init_graphic(this.tile_container);

			for (let neigh of this.adjacent_tiles(next)) {
				if (this.tiles[neigh] === undefined) {
					to_add.push(neigh);
				}
			}
		}
	}


	// this is the function that determines the shape & size of the board
	is_coord_on_board(hex_coord)
	{
		if (!is_legit_coord(hex_coord)) {
			return false;
		}
		if (Math.abs(hex_coord[0]) > 4) {
			return false;
		}
		if (Math.abs(hex_coord[1]) > 4) {
			return false;
		}
		if (Math.abs(hex_coord[2]) > 3) {
			return false;
		}
		return true;
	}

	adjacent_tiles(hex_coord)
	{
		let adj = [];
		for (let neigh_delta of ADJ_HEXES) {
			let neigh = [ hex_coord[0]+neigh_delta[0], hex_coord[1]+neigh_delta[1], hex_coord[2]+neigh_delta[2] ];
			if (this.is_coord_on_board(neigh)) {
				adj.push(neigh);
			}
		}
		return adj;
	}


	try_play_card_on(card, tile)
	{
		console.log("playing on tile", tile)
		if (tile.unit && card.data.unit) {
			// tile has unit so cannot play unit
			return false;
		} else {
			console.log(tile.coords)
			networking.send_as_json({
				type: "M_play_card",
				uuid: card.data.uuid,
				coords: tile.coords,
			});
			return true;
		}
	}


}





