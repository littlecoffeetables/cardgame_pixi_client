

class Unit
{
	constructor(card_data, owner, tile, unit_stats)
	{
		this.card = card_data;
		this.owner_id = owner;
		if (owner === match.my_player_number) {
			this.is_friendly = true;
		} else {
			this.is_friendly = false;
		}
		this.tile = tile;
		this.tile.unit = this;
		this.stat_displays = {};
		this.override_stats = unit_stats || {};
		this.sprite = this.make_unit_sprite();
		this.tile.update_tint();
		match.board.units[this.uuid] = this;
	}

	get uuid()
	{
		return this.card.uuid;
	}

	destroy()
	{
		delete match.board.units[this.uuid];
		this.tile.unit = null;
		this.tile.graphic.removeChild(this.sprite);
		this.tile.update_tint();
		if (this.tile === matchui.selected_tile) {
			matchui.deselect_everything();
		}
	}

	can_move()
	{
		return this.override_stats.speed > 0;
	}

	can_attack()
	{
		return this.override_stats.speed > 0;
	}

	try_move(to_tile)
	{
		networking.send_as_json({
			"type": "M_unit_move",
			"uuid": this.uuid,
			"to": to_tile.coords
		});
	}

	try_attack(unit)
	{
		networking.send_as_json({
			"type": "M_unit_attack",
			"uuid": this.uuid,
			"target": unit.uuid
		})
	}

	move(from_tile, to_tile)
	{
		from_tile.unit = null;
		to_tile.unit = this;
		this.tile = to_tile;
		from_tile.graphic.removeChild(this.sprite);
		from_tile.update_tint();
		to_tile.graphic.addChild(this.sprite);
		to_tile.update_tint();
		if (from_tile === matchui.selected_tile) {
			matchui.deselect_everything();
			matchui.select_tile(to_tile);
		}
	}

	stat_change(data)
	{
		const stat_name = data.stat;
		if (this.stat_displays[stat_name]) {
			this.stat_displays[stat_name].text = data.new_value;
		}
		this.override_stats[stat_name] = data.new_value;
	}




	make_unit_sprite()
	{
		// placeholder texture
		let texture = PIXI.utils.TextureCache[graphical.images.gem_r];
		let sprite = new PIXI.Sprite(texture);
		this.tile.graphic.addChild(sprite);
		sprite.anchor.set(0.5, 0.5);

		let stats_ = [
			//{ name:"atk", stat:this.card.data.stats.atk, pos:[-25,25], options:{fill:"orange", fontSize:12} },
			{ name:"atk", stat:this.override_stats.atk, pos:[-25,25], options:{fill:"orange", fontSize:14} },
			{ name:"speed", stat:this.override_stats.speed, pos:[0,25], options:{fill:"cyan", fontSize:14} },
			{ name:"hp", stat:this.override_stats.hp, pos:[25,25], options:{fill:"red", fontSize:14} },
		];
		for (let i = 0; i < stats_.length; ++i) {
			let stat = stats_[i];
			if (stat.stat !== undefined) {
				let text = new PIXI.Text(""+stat.stat, stat.options);
				sprite.addChild(text);
				text.position.set(stat.pos[0], stat.pos[1]);
				let anchor = stat.anchor || [0.5, 0.5];
				text.anchor.set(anchor[0], anchor[1]);
				if (stat.name) {
					this.stat_displays[stat.name] = text;
				}
			}
		}

		return sprite;
	}


}


