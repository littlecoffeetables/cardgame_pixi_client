

matchui = {};

matchui.create_hex_tile_graphic = function(tile, colour, outline, outline_thickness)
{
	// path clockwise in a spiky-top hex shape starting at the top
	let a = graphical.cos30 * tile.scale, b = graphical.cos60 * tile.scale, c = 1 * tile.scale;
	let path = [
		0, -c,
		a, -b,
		a, b,
		0, c,
		-a, b,
		-a, -b
	]
	let hex = new PIXI.Graphics();
	hex.beginFill(colour);
	hex.lineStyle(outline_thickness, outline, 1);
	hex.drawPolygon(path);
	hex.endFill();
	hex.closePath();

	hex.tile = tile;

	hex.interactive = true;
	hex.interactiveChildren = false;
	hex.buttonMode = true;
	hex.on("mouseover", function(evt) {
		tile.graphic.addChild(matchui.main_hud.hover_tile_highlighter);
	})
	hex.on("mouseout", function(evt) {
		tile.graphic.removeChild(matchui.main_hud.hover_tile_highlighter);
	})
	hex.on("click", function(evt) {
		mainui.just_clicked = true;
		matchui.handle_tile_click(hex.tile);
	})

	return hex;
}

matchui.handle_tile_click = function(tile)
{
	if (matchui.selected_card) {
		if (match.board.try_play_card_on(matchui.selected_card.card, tile)) {
			matchui.selected_card.tint = 0x333333;
			matchui.deselect_card();
		}
	} else {
		matchui.select_tile(tile);
	}
}

matchui.selected_tile = null;
matchui.deselect_tile = function()
{
	let tile = matchui.selected_tile;
	if (tile) {
		tile.graphic.is_selected_hex = false;
		tile.graphic.removeChild(matchui.main_hud.sel_tile_highlighter);
		matchui.selected_tile = null;
		matchui.unzoom();
		matchui.clear_arrows();
	}
}
matchui.select_tile = function(tile)
{
	if (tile === matchui.selected_tile) {
		matchui.deselect_tile();
	}
	else {
		if (matchui.selected_tile && matchui.selected_tile.unit && matchui.selected_tile.unit.is_friendly && match.is_my_turn()) {
			matchui.try_move_or_attack(matchui.selected_tile, tile);
		} else {
			matchui.actually_select_tile(tile);
		}
	}
}
matchui.actually_select_tile = function(tile)
{
	matchui.deselect_tile();
	matchui.selected_tile = tile;
	tile.graphic.is_selected_hex = true;
	tile.graphic.addChild(matchui.main_hud.sel_tile_highlighter);
	if (tile.unit) {
		matchui.make_zoomed(tile.unit.card);
		if (match.is_my_turn() && tile.unit.is_friendly) {
			matchui.create_arrows(tile);
		}
	}
}

matchui.arrows = [];
matchui.create_arrows = function(tile)
{
	if (!tile.unit) {
		return;
	}
	const move_colour = 0xff9966, attack_colour = 0xff0000;
	let angle = -60;
	for (let dir of ADJ_HEXES) {
		angle += 60;
		let new_coords = tile.get_offset_coords(dir);
		if (match.board.is_coord_on_board(new_coords)) {
			let adj_tile = match.board.tiles[new_coords];
			let colour = move_colour;
			if (adj_tile.unit) {
				if (adj_tile.unit.is_friendly || !tile.unit.can_attack()) {
					continue;
				} else {
					colour = attack_colour;
				}
			} else {
				if (!tile.unit.can_move()) {
					continue;
				}
			}
			let arrow = new PIXI.Sprite(PIXI.utils.TextureCache[graphical.images.arrow]);
			arrow.tint = colour;
			arrow.anchor.set(0.5,0.5);
			arrow.angle = angle;
			arrow.alpha = 0.5;
			adj_tile.graphic.addChild(arrow);
			matchui.arrows.push(arrow);
		}
	}
}
matchui.clear_arrows = function()
{
	for (let arrow of matchui.arrows.splice(0, matchui.arrows.length)) {
		arrow.parent.removeChild(arrow);
		arrow.destroy();
	}
}

matchui.try_move_or_attack = function(from_tile, to_tile)
{
	if (!( from_tile && from_tile.unit /*&& from_tile.unit.is_friendly && match.is_my_turn()*/ ) || (to_tile.unit && to_tile.unit.is_friendly)) {
		console.log("sanity check failed");
		matchui.deselect_tile();
		return;
	}
	if (!from_tile.is_adjacent_with(to_tile)) {
		matchui.deselect_tile();
		return;
	}
	if (to_tile.unit) {
		from_tile.unit.try_attack(to_tile.unit);
	} else {
		from_tile.unit.try_move(to_tile);
	}
}

matchui.deselect_everything = function()
{
	matchui.deselect_card();
	matchui.deselect_tile();
}



matchui.select_card = function(card_sprite)
{
	if (matchui.selected_card) {
		matchui.selected_card.deselect();
	}
	matchui.deselect_tile()
	card_sprite.select()
	matchui.selected_card = card_sprite;
}
matchui.deselect_card = function()
{
	if (matchui.selected_card) {
		matchui.selected_card.deselect();
		matchui.selected_card = null;
		matchui.unzoom();
	}
}

matchui.make_zoomed = function(card)
{
	card.make_sprite({existing_sprite: matchui.zoomed_card_sprite});
}
matchui.unzoom = function()
{
	matchui.zoomed_card_sprite.texture = null;
	matchui.zoomed_card_sprite.removeChildren();
}

matchui.opponent_hovered_sprite = null;
matchui.opponent_hover = function(data) {
	let index = 0;
	for (card_c of match.opponent.hand.container.children) {
		if (index === data.index) {
			card_c.children[0].opponent_hover();
		} else {
			card_c.children[0].opponent_endhover();
		}
		++index;
	}
}
matchui.opponent_endhover = function(data) {
	match.opponent.hand.get_sprite_at(data.index).opponent_endhover();
}

matchui.create_hud = function()
{
	let hud = {};
	hud.container = new PIXI.Container();

	let BR_container = new PIXI.Container();
	let endturn_button = graphical.create_button("...", match.end_turn);
	endturn_button.position.set(-endturn_button.width/2, -endturn_button.height/2);
	BR_container.addChild(endturn_button);
	hud.end_turn_button = endturn_button;
	graphical.add_resize_reposition(BR_container, function(w,h) {
		this.position.set(w, h);
	});
/*
	let TR_container = new PIXI.Container();
	let tr_text = new PIXI.Text("TR_container", {fill:"white", fontSize:24});
	tr_text.position.set(-tr_text.width, 0);
	TR_container.addChild(tr_text);
	graphical.add_resize_reposition(TR_container, function(w,h) {
		this.position.set(w, 0);
	});
*/
	hud.sac_container = new PIXI.Container();
	const sac_order = ['cards', 'red', 'blue', 'green', 'neutral'];
	const sac_count = sac_order.length;
	const offset = 40;
	let current_x = -offset * sac_count / 2;
	for (let sac_reward of sac_order) {
		let back = new PIXI.Sprite(PIXI.utils.TextureCache[graphical.images.sac_back]);
		let texture_name = (sac_reward === "cards") ? "sac_cards" : graphical.gems[sac_reward];
		let sac_sprite = new PIXI.Sprite(PIXI.utils.TextureCache[graphical.images[texture_name]]);
		hud.sac_container.addChild(back);
		back.addChild(sac_sprite);
		back.interactive = true;
		back.interactiveChildren = false;
		back.buttonMode = true;
		back.on("mouseout", function() {
			back.tint = 0xffffff;
		});
		back.on("mouseover", function() {
			back.tint = 0xff9966;
		});
		back.on("click", function() {
			matchui.try_sacrifice(sac_reward);
		});
		back.position.set(current_x, 0);
		current_x += offset;
	}
	hud.sac_container.position.set(0, -200);

	hud.sel_tile_highlighter = new PIXI.Sprite(PIXI.utils.TextureCache[graphical.images.tile_highlighter]);
	hud.sel_tile_highlighter.anchor.set(0.5,0.5);
	hud.sel_tile_highlighter.tint = 0xff9966;
	hud.hover_tile_highlighter = new PIXI.Sprite(PIXI.utils.TextureCache[graphical.images.tile_highlighter]);
	hud.hover_tile_highlighter.anchor.set(0.5,0.5);
	hud.hover_tile_highlighter.tint = 0xff6699;

	hud.container.addChild(BR_container/*, BL_container, TR_container, TL_container*/);
	return hud;
}

matchui.try_sacrifice = function(sac_for)
{
	if (match.current_player === match.my_player_number && matchui.selected_card && matchui.can_sacrifice) {
		matchui.selected_card.card.try_sacrifice(sac_for);
	}
}


matchui.change_turn = function()
{
	if (match.current_player === match.my_player_number) {
		matchui.main_hud.end_turn_button.enable("End Turn");
		matchui.can_sacrifice = true;
	} else {
		matchui.main_hud.end_turn_button.disable("Opponent Turn");
	}
}


matchui.end_match = function(result)
{
	if (matchui.game_over_screen === undefined) {
		matchui.init_end_match();
	}
	let result_text = {"win":"You won!", "loss":"You lost", "draw":"Draw!", "dc":"Connection lost"}[result];
	if (result_text === undefined) {
		result_text = "Match over for unknown reason";
	}
	matchui.game_over_info_text.text = result_text;
	app.stage.addChild(matchui.game_over_screen);
	graphical.resize_app();
}

matchui.init_end_match = function()
{
	// display gameover screen
	matchui.game_over_screen = new PIXI.Container();
	graphical.add_resize_reposition(matchui.game_over_screen, function(w, h, s) {
		this.position.set(w / 2, h / 2);
	});
	let return_button = graphical.create_button("Return to lobby", mainui.return_to_lobby);
	return_button.position.set(0, 100);
	matchui.game_over_screen.addChild(return_button);
	let info_text = new PIXI.Text("", {fontSize:50, fill:"gold"});
	matchui.game_over_info_text = info_text;
	graphical.add_resize_reposition(info_text, function(w,h,s) {
		this.position.set(-this.width / 2, -100);
	});
	matchui.game_over_screen.addChild(info_text);
}

matchui.start_match = function(match)
{
	app.stage.removeChild(lobbyui.lobby_container);
	matchui.match_container = new PIXI.Container();
	/*graphical.gem_containers = [new PIXI.Container(), new PIXI.Container()];
	graphical.match_container.addChild(graphical.gem_containers[0], graphical.gem_containers[1]);*/
	matchui.match_container.addChild(match.you.hand.container, match.opponent.hand.container);
	matchui.match_container.addChild(match.you.hud.container, match.opponent.hud.container);

	matchui.zoomed_card_sprite = new PIXI.Sprite();
	graphical.add_resize_reposition(matchui.zoomed_card_sprite, function(w, h) {
		this.position.set(w * 0.8, h/2);
	});
	matchui.match_container.addChild(matchui.zoomed_card_sprite);
	matchui.zoomed_card_sprite.on("click", function(evt) {
		mainui.just_clicked = true;
	});
	matchui.main_hud = matchui.create_hud();
	matchui.match_container.addChild(matchui.main_hud.container);
	app.stage.addChild(matchui.match_container);
	matchui.selected_card = null;
	matchui.match_container.addChild(match.board.tile_container);
	graphical.add_resize_reposition(match.board.tile_container, function(w,h,s) {
		graphical.centre(this);
		this.scale.set(s);
	});

	graphical.resize_app();

	networking.set_data_handler("P_hover", matchui.opponent_hover);
	networking.set_data_handler("P_endhover", matchui.opponent_endhover);

	matchui.change_turn();
}

matchui.hide = function()
{
	app.stage.removeChild(matchui.match_container);
	app.stage.removeChild(matchui.game_over_screen);
}

matchui.show = function()
{
	app.stage.addChild(matchui.match_container);
	//app.stage.addChild(matchui.game_over_screen);
}

