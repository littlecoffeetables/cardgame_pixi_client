// for now, the main script

var match;
var networking;

function start_match(data) {
	match = new Match(data);
	matchui.start_match(match);
	console.log("Match has started!")
}

function init_game() {
	// note to self: this may need to wait to run until assets are loaded
	mainui.init();
	chat.init(networking);
	lobby.init(networking, start_match);
	lobbyui.init_lobby(lobby);
	if (!login.user_id) {
		console.error("Did not get a user id!")
	}
}

function init() {
	networking = new Networking("localhost", 8081);
	// login and chat are not classes, at least for now
	login.init(networking, init_game);
}
window.onload = init;

