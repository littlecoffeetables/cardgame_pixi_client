

class Hand
{
	constructor(player)
	{
		this.player = player;
		this.container = new PIXI.Container();
		this.contents = [];

		if (this.player.is_opponent) {
			graphical.add_resize_reposition(this.container, function(w, h, s) {
				this.position.set(w/2, -80 * s);
				this.scale.set(s);
			});
		} else {
			graphical.add_resize_reposition(this.container, function(w, h, s) {
				this.position.set(w/2, h + 50 * s);
				this.scale.set(s);
			});
		}
	}

	size()
	{
		return this.contents.length;
	}

	get_sprite_at(index)
	{
		return this.get_container_at(index).children[0];
	}
	get_container_at(index)
	{
		return this.container.children[index];
	}

	get_index_of(uuid)
	{
		if (this.player.is_opponent) {
			return -1;
		}
		let index = 0;
		for (card of this.contents) {
			if (card.uuid === uuid) {
				return index;
			}
			++index;
		}
		return -1;
	}

	add_card(card, i)
	{
		card.put_to_hand(this);
		this.contents.push(card);
		let card_container = new PIXI.Container();
		card_container.addChild(card.graphic);
		this.container.addChild(card_container);
		// so it slides in nicely from the right
		card_container.x = 2000 + (i || 0) * 3000;
		this.respace();
	}

	make_and_add_card(card_data, i)
	{
		let facedown = (this.player.is_opponent ? true : false);
		let card = new Card(card_data, facedown);
		this.add_card(card, i);
	}

	rm_card(uuid)
	{
		if (uuid === undefined) {
			this.contents.pop();
			this.container.removeChildAt(0);
		} else {
			matchui.deselect_card();
			let index = 0;
			for (let card of this.contents) {
				if (card.uuid === uuid) {
					this.contents.splice(index, 1);
					this.container.children[index].children[0].anim_function = null;
					this.container.children[index].destroy({children:true});
					break;
				}
				++index;
			}
		}
		this.respace();
	}

	respace() {
		let card_count = this.size();
		let spacing = 1500 / (card_count + 3);
		let next_x = 0 - spacing * (card_count - 1) / 2;
		let tilt_spacing = Math.PI / 60; // 3 degrees
		let tilt = 0 - tilt_spacing * (card_count - 1) / 2;
		let dir_ = (this.player.is_opponent ? -1 : 1);
		for (let card_c of this.container.children) {
			let has_card = false;
			// these sanity checks seem to be required or firefox freaks out
			if (card_c.children && card_c.children.length > 0) {
				let card_sprite = card_c.children[0];
				if (card_sprite.set_target_x !== undefined) {
					has_card = true;
					let diff = card_c.x - next_x;
					card_sprite.x += diff;
					card_sprite.set_target_x(0);
				}
			}
			card_c.x = next_x;
			next_x += spacing;
			if (has_card) {
				card_c.rotation = dir_ * tilt;
				card_c.y = dir_ * (1 - Math.cos(tilt * 4)) * card_c.children[0].height / 2;
				let x_diff = (Math.sin(tilt)) * card_c.children[0].width / 2;
				card_c.x -= x_diff;
				next_x -= x_diff;
				tilt += tilt_spacing;
			}
		}
	}


}

